import java.util.*;
import java.text.*;
 
public class Alphabetizer  {
	String englishRules;
	String smallnTilde;
	String capitalNTilde;
	public Alphabetizer(){
		englishRules = 
         ("< a,A < b,B < c,C < d,D < e,E < f,F " +
          "< g,G < h,H < i,I < j,J < k,K < l,L " +
          "< m,M < n,N < o,O < p,P < q,Q < r,R " +
          "< s,S < t,T < u,U < v,V < w,W < x,X " +
          "< y,Y < z,Z");
   
      smallnTilde  = new String("\u00F1");
      capitalNTilde = new String("\u00D1");
		
		
		
		}
 
   public static void sortStrings(Collator collator, ArrayList<ShiftedTitle> list) {
       ShiftedTitle tmp;
       for (int i = 0; i < list.size(); i++) {
           for (int j = i + 1; j < list.size(); j++) {
               // Compare elements of the words array
               if( collator.compare(list.get(i).getName(), list.get(j).getName() ) > 0 ) {
                   // Swap words[i] and words[j] 
                   tmp = list.get(i);
                   list.set(i,list.get(j));
                   list.set(j,tmp);
               }
           }
       }
   }
 
   public ArrayList<ShiftedTitle> alphabetize(ArrayList<ShiftedTitle> list) {
 
      
 
       try {
           RuleBasedCollator enCollator = 
              new RuleBasedCollator(englishRules);
             
           sortStrings(enCollator, list);
           
       } catch (ParseException pe) {
           System.out.println("Parse exception for rules");
       }
       return list;
   }
}