import java.util.*;

public class MasterController {
	private ArrayList<ShiftedTitle> shiftedTitles;
	private ArrayList<Title> titles;
	private Alphabetizer alp;
	private OutPut out;
	
	public MasterController(){
		shiftedTitles = new ArrayList<ShiftedTitle>();
		alp= new Alphabetizer();
		out=new OutPut();
		titles= new ArrayList<Title>();
		
		//LogicalController lController=new LogicalController(TreeSet ts);
		}	
	public void addElement(String s){
		ShiftWord(s);
		titles.add(new Title(s));
		
		}
	public void showShiftedList(){
		out.list(shiftedTitles);
		}
	public void clear(){
		shiftedTitles.clear();
		out.clear();
		}		

   /* //================================================//
   						SHIFT MODULE
   	  //================================================//

   */
      
   /**
         * ShiftWord: This function is going to send all the different shifted lines to the alphabetizer
         * 
         * @param title
         *            The object title that might be shifted and sent to the TreeSet 
         * 
         * @param ts
         *            The TreeSet who wil organize the shifted titles alphabetically 
                  */
   public void ShiftWord(String s){
    String[] result = s.split(" ");
    int i=result.length;
    titles.add(new Title(s));
    for(int j=0;j<i;j++){
    	    if(IgnoreWords(result[j])){//this boolean must compare the word to a list
    		String completeWord=result[j];
    		for(int u=j+1;u<i;u++){
    			completeWord= completeWord+" "+ result[u];
    			}
    		for(int u=0;u<j;u++){
    			completeWord= completeWord+" "+ result[u];
    			}
    		shiftedTitles.add(new ShiftedTitle(completeWord,titles.get(titles.size()-1)));
    		alp.alphabetize(shiftedTitles);    		
    		}
    	}    
   	}
   	
   /**
         * Ignore words: This function Ignore the words to ignore while shifting the
         original title.
         * 
         * @param s
         *            The word who must be tested and compared to the list of words to Ignore
         */
   public static Boolean IgnoreWords(String s){
   	Boolean test=true;
   	String[] WordsToIgnore={"The","As","the","is","of","and","as","a","after"};//List with the words to Ignore
   	int i=WordsToIgnore.length;
   	for(int j=0;j<i;j++){
   		if(s.equals(WordsToIgnore[j])){
   			test=false;
   		}
   	}
   	return test;
   }
   public void afficheTitles(){
   	String text="";
   	for(int i=0;i<titles.size();i++){
        	text=text+titles.get(i).getName()+"\n";
        	}
        	System.out.println(text);
   }
}
