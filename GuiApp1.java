//Imports are listed in full to show what's being used
//could just import javax.swing.* and java.awt.* etc..
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GuiApp1 {
	public static void main(String args[]) {		
		ArrayList list= new ArrayList();
		 
		GuiApp1 guiApp1=new GuiApp1(list);
		
	}
    TextField textField;
    TextArea textArea;
    JFrame guiFrame;
    Panel panelSouth;
    Panel panelNorth;
    MasterController mController;
    public GuiApp1(ArrayList list)
    {
      mController=new MasterController();
      guiFrame = new JFrame();
 	  panelSouth = new Panel(new FlowLayout());
 	  panelNorth = new Panel(new FlowLayout());
 	  textField = new TextField(20);
 	  textArea=new TextArea();
 	  
 	  guiFrame.add(panelSouth,BorderLayout.SOUTH);
 	  guiFrame.add(panelNorth,BorderLayout.NORTH);
 	  guiFrame.add(textArea,BorderLayout.CENTER);
      panelNorth.add(new Label("Enter Text: "));
      panelNorth.add(textField);
 
        
        //make sure the program exits when the frame closes
        guiFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        guiFrame.setTitle("Example GUI");
        guiFrame.setSize(400,500);
        textArea.setEditable(false);//We cannot write directly into this area
      
        //This will center the JFrame in the middle of the screen
        guiFrame.setLocation(200,150);        
        JButton generateBut = new JButton( "Generate List");
        JButton addBut = new JButton( "Add a title");
        JButton deleteBut = new JButton( "Clear All");
        
                
        //The ActionListener class is used to handle the
        //event that happens when the user clicks the button.
        //As there is not a lot that needs to happen we can 
        //define an anonymous inner class to make the code simpler.
        generateBut.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent event)
            {
             	mController.showShiftedList();  

            }
        });
        addBut.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent event)
            {
            	if(textField.getText().equals("")==false){
               		textArea.append(textField.getText()+"\n");
               		mController.addElement(textField.getText());              		
               		textField.setText("");
            	}

            }
        });
        deleteBut.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent event)
            {
               mController.clear();
               textArea.setText("");

            }
        });
        
        panelSouth.add(addBut);
        panelSouth.add(generateBut);
        panelSouth.add(deleteBut);
        
        
        //make sure the JFrame is visible
        guiFrame.setVisible(true);
        
    }
    
}