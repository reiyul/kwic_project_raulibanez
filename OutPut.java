import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class OutPut {
    TextArea textArea;
    JFrame guiFrame;
    String text;
    
    public OutPut()
    {
      text="";
      guiFrame = new JFrame();
 	  
 	  textArea=new TextArea(); 
 	  guiFrame.add(textArea,BorderLayout.CENTER);
        
        //make sure the program exits when the frame closes
        guiFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        guiFrame.setTitle("Shifted List");
        guiFrame.setSize(400,500);
        textArea.setEditable(false);//We cannot write directly into this area
      	
        //This will center the JFrame in the middle of the screen
        guiFrame.setLocation(650,150);        
        
        
        
        //make sure the JFrame is visible
        guiFrame.setVisible(true);
        
    }
    
    public void list(ArrayList<ShiftedTitle> outPuts){
    	/*
    String[] result = outPuts.get(1).split(" ");
    for(int j=0;j<result.length;j++){
    	    if(true){//this boolean must compare the word to a list
    		String completeWord=result[j];
    		for(int u=j+1;u<result.length;u++){
    			completeWord= completeWord+" "+ result[u];
    			}
    		for(int u=0;u<j;u++){
    			completeWord= completeWord+" "+ result[u];
    			}
    		    		
    		}
    	}    
    	*/
    	
    	for(int i=0;i<outPuts.size();i++){
        	text=text+outPuts.get(i).getName()+"\n";
        	}
    	textArea.setText(text);
    }
    public void clear(){
    	text="";
    	textArea.setText("");
    }
    
}